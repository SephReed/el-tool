export function byId(id): HTMLElement { return document.getElementById(id); }

// adds and removes classes.  can take a string with spaces for mutiple classes
export function classSplice(
	element: HTMLElement,
	removeClasses: string | string[],
	addClasses: string | string[],
): HTMLElement {
	if (removeClasses) {
		const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
		remList.forEach((className) => element.classList.remove(className));
	}
	if (addClasses) {
		const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
		addList.forEach((className) => element.classList.add(className));
	}
	return element;
}
export function addClass(element: HTMLElement, addClasses: string): HTMLElement {
	return classSplice(element, undefined, addClasses);
}

// remove all children from element
export function removeChildren(parent: HTMLElement) {
	let child = parent.firstChild;
	while( !!child ) {
		parent.removeChild(child);
		child = parent.firstChild;
	}
}

export type Child = string | HTMLElement | number | null | undefined;
export type Innards = string | Array<Child | (() => Child)>;

export function setInnards(root: HTMLElement, innards: Innards) {
	removeChildren(root);
	append(root, innards);
}

// Append stuff to an element.
// Takes 1. innerHTML as string,
// 2. an array of elements/strings where the text becomes a Text element,
// or 3. a function which returns an element or string
export function append(
	root: HTMLElement,
	innards: Innards,
) {
	if (innards === undefined) { return }
	if (Array.isArray(innards)) {
		innards.forEach((child) => {
			if (typeof child === "function") {
				child = child();
			}
			if (child === undefined || child === null) {
				return;
			}
			if (typeof child === "number") {
				child = child + "";
			}
			if (typeof child === "string") {
				root.appendChild(new Text(child))
			} else {
				root.appendChild(child as any); // numbers turn to string above
			}
		});
	} else {
		root.innerHTML += innards;
	}
}

export interface IProps {
	class?: string;
	className?: string;
	classList?: string[];
	text?: string;
	innerHTML?: string;
	id?: string;
	attr?: {[key: string]: string};
	style?: {[key: string]: string} | string;
	on?: {[key: string]: (event: Event) => boolean | void | any};
	onClick?: (event: MouseEvent) => boolean | void | any;
	onMouseDown?: (event: MouseEvent) => boolean | void | any;
	onKeyDown?: (event: KeyboardEvent) => boolean | void | any;
	prependTo?: HTMLElement;
	appendTo?: HTMLElement;
	this?: {
		[key: string]: any;
	}
}

export function create<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	propsOrInnards?: IProps | string | Array<string | HTMLElement>,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	const out = document.createElement(tagName);
	let props: IProps;
	if (propsOrInnards) {
		if (innards)  {
			if (typeof propsOrInnards !== "object") {
				throw new Error("El.create() has third argument, but props is not an object");
			}
			props = propsOrInnards as any;
		} else if (typeof propsOrInnards === "object" && !Array.isArray(propsOrInnards)) {
			props = propsOrInnards;
		} else {
			innards = propsOrInnards;
		}
	}

	if (props) {
		// event stuff
		if (props.on) {
			const eventListeners = props.on || {};
			Object.keys(eventListeners).forEach((eventType) =>
				out.addEventListener(eventType, eventListeners[eventType])
			);
		}
		if (props.onClick) { out.addEventListener("click", props.onClick); }
		if (props.onKeyDown) { out.addEventListener("keydown", props.onKeyDown); }
		if (props.onMouseDown) { out.addEventListener("mousedown", props.onMouseDown); }

		// element html props, and element js props
		if (props.attr) {
			const attrs = props.attr || {};
			Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
		}
		if (props.this) {
			const thisProps = props.this || {};
			Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
		}

		// prop style content
		if (props.text) { out.textContent += props.text; }
		if (props.innerHTML) { out.innerHTML += props.innerHTML; }

		// classing
		const classList = props.classList || [];
		if (props.className) { classList.push(props.className) }
		if (props.class) { classList.push(props.class) }
		out.className =  classList.join(" ");
		if (props.id) { out.id = props.id; }


		// inline styles
		if (props.style) {
			if (typeof props.style === "string") {
				out.style.cssText = props.style;
			} else Object.keys(props.style).forEach((styleName) => out.style[styleName] = props.style[styleName]);
		}
	}

	// add all chidren, or innards
	if (innards) { append(out, innards); }

	// append to parent at the end, so multiple reflows are not triggered
	if (props) {
		if (props.appendTo) {
			props.appendTo.appendChild(out);
		}
		if (props.prependTo) {
			const parent = props.prependTo;
			parent.insertBefore(out, parent.firstChild);
		}
	}

	return out;
}

export function hr() { return document.createElement("hr"); }
export function br() { return document.createElement("br"); }

// export function div(className: string, props: any): HTMLDivElement;
// export function div(props: any): HTMLDivElement;
export function div(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLDivElement {
	return shorthand("div", classNameOrProps, propsOrInnards, innards)
}

export function seg(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLDivElement {
	return shorthand("seg", classNameOrProps, propsOrInnards, innards);
}

export function span(
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLSpanElement {
	return shorthand("span", classNameOrProps, propsOrInnards, innards)
}

function shorthand<K extends keyof HTMLElementTagNameMap> (
	tagName: K,
	classNameOrProps?: string | IProps,
	propsOrInnards?: IProps | Innards,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	let props: IProps;
	if (typeof classNameOrProps === "object") {
		props = classNameOrProps;
	} else if (typeof propsOrInnards === "object" && !Array.isArray(propsOrInnards)) {
		props = propsOrInnards;
	} else {
		props = {};
	}

	if (Array.isArray(propsOrInnards) || typeof propsOrInnards == "string") {
		innards = propsOrInnards;
	}

	if (typeof classNameOrProps === "string") {
		props.className = (props.className ? props.className + " " : "") + classNameOrProps;
	}
	return create(tagName, props, innards);
}

document.head.appendChild(create("style", {id: "styler-seg"}, `
	seg { display: inline-block; }
`))
