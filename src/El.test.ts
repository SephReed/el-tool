import * as El from "./El";

describe("Testing El", () => {
	it("should create componets without props correctly", () => {
		const testy = El.create("span");
		expect(testy.tagName).toBe("SPAN");
	});

	it("should create componets with props correctly", () => {
		const testy = El.create("div", { id: "testy", className: "testyClass" });
		expect(testy.id).toBe("testy");
		expect(testy.className).toBe("testyClass");
	});

	it("should append children correctly", () => {
		const testy = El.div();
		const child2 = El.div("Child2", {appendTo: testy});
		expect((testy.childNodes[0] as HTMLElement).classList.contains("Child2")).toBe(true);
		const child1 = El.div("Child1", {prependTo: testy});
		expect((testy.childNodes[0] as HTMLElement).classList.contains("Child1")).toBe(true);
		expect((testy.childNodes[1] as HTMLElement).classList.contains("Child2")).toBe(true);

		const hasChildren = El.div({}, [El.div("testChild")]);
		expect(hasChildren.firstElementChild.className).toBe("testChild");
	});

	it("should shorthand well", () => {
		const testy = El.div("Eden", [
			El.div("Adam", {}, "hey, what up"),
			El.div("Eve", "man, I'm hungry"),
		]);
		expect(testy.childElementCount).toBe(2);

	});
});
