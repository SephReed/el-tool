# EL-tool

El-tool is a tersifier for creating DOM nodes and adding event listeners.  It is not a library, it just shortens some rather verbose native code.

<br>

## QRE (quick reference example)
The following are the same:

***Vanilla.js*** (730 char)
<pre>
const root = document.createElement("div");
root.className = "Root";

const topbar = document.createElement("div");
topbar.className = "Topbar";
root.appendChild(topbar);

const sidebar = document.createElement("div");
sidebar.className = "Sidebar";
root.appendChild(sidebar);

for (let i = 0; i < 10; i++) {
	const sidebarItem = document.createElement("span");
	sidebarItem.className = "SidebarItem";
	sidebarItem.id = i;
	sidebarItem.innerText = `Link #${i}`;
	sidebarItem.addEventListener("click", () => {
		content.innerText = sidebarItem.id;
	})
	sidebar.appendChild(sidebarItem);
}

const content = document.createElement("div");
content.className = "Content";
content.innerText = "Starting Text";
root.appendChild(content);
</pre>

***El.ts*** (331 char)

<pre>
import {div, span} from "@coder/el"

const ten = [...Array(10).keys()];

let content;
const root = div("Root", [
	div("Topbar"),
	div("Sidebar", 
		ten.map((id) =>
			span("SidebarItem", {
					id, 
					onClick: () => content.innerText = id,
				},
				`Link #${id}`;,
			),
		),
	),
	content = div("Content", "StartingText"),
]);
</pre>

<br><br><br>

## Benefits

* Currently the fastest possible way to render an interactive DOM (see benchmarks section below)
* Nested syntax reflects DOM nesting
* < 250 lines of code (with comments, unminified)
* Significantly reduces redundancy in code
* Super easy DOM node reference catching (no queries)

<!-- 
<br><br><br>

## Creating Elements

`El.create(tagName, props?)`

`El.div(className, props?)`

`El.div(props?)`


***Props*** is an object containing any attributes you'd like to apply to the element.  ie.

<pre>
const hat = El.div({
	areYouAFez: "yes",
	setColor: (color) => hat.color = color,
});
console.log(hat.areYouAFez) // outputs: yes
hat.setColor("red");
console.log(hat.color); // outputs: red
</pre>

***Props*** also has the following self-explanatory typechecked properties (which do typecheck correctly in the typescript playground, but not in VSCode)...

<pre>
class?: string;
className?: string;
classList?: string[];
prependTo?: HTMLElement;
appendTo?: HTMLElement;
</pre>

...and these slightly special properties:

* `attr?: {[key: string]: string}`: An object of string attributes which will become part of the tag ie.<pre>
const boots = El.div({attr: {witDaFur: "get low"}});
// renders to &lt;div witDaFur="get low">&lt;/div>
</pre>

* `on?: {[key: string]: () => boolean}`: A list of event listeners<pre>
const loudCow = El.div({on: {click: () => alert("MOO!")}});
loudCow.click(); // outputs: "MOO!"
</pre>


<br><br><br> -->

## Helper Functions

`byId(id)` : tersifies as `document.getElementById(id)`

`classSplice(element: HTMLElement, removeClasses: string | string[], addClasses: string | string[])` : tersifies `element.classList.add()` and `element.classList.remove()`

`removeChildren(parent: HTMLElement)` : removes all children
